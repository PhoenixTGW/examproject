﻿using Examproject.Commands;
using Examproject.Database;
using Examproject.PlayerThings;
using Examproject.States;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Examproject
{
    public class GameWorld : Game
    {
        private readonly GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        public static ContentManager contentFix { get; set; } // Used to run separate loading programs to divide up the work.
        public static float DeltaTime;
        private InputHandler inputHandler;

        public static Player player = new Player(new Vector2(0, 0));
        //private Background background = new Background();

        public static List<GameObject> ObjectList;
        public static List<GameObject> newObjectList;
        public static List<GameObject> deletionList;

        private SpriteFont font;
        private readonly string displayText = "Hello, this is a test";

        //                  Nikolaj stuff
        private State _currentState;
        private State _nextState;

        public void ChangeState(State state)
        {
            _nextState = state;
        }

        public GameWorld()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            _graphics.PreferredBackBufferWidth = 1600;  // The window width. Do not go above 1600
            _graphics.PreferredBackBufferHeight = 900;   // Window height. Do not go above 900
            _graphics.ApplyChanges();
        }

        protected override void Initialize()
        {
            IsMouseVisible = true;
            DatabaseSetup.setupDatabase();
            inputHandler = new InputHandler();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice); // Always load this first.


            //                  Nikolaj Stuff
            _currentState = new MenuState(this, _graphics.GraphicsDevice, Content);

            ObjectList = new List<GameObject>()
            {
                player
            };
            foreach (GameObject O in ObjectList)
            {
                O.LoadContent(Content);
            }
            //background.LoadContent(Content);
            contentFix = Content;
            deletionList = new List<GameObject>();
            newObjectList = new List<GameObject>();
            font = Content.Load<SpriteFont>("Fonts/Text"); // Use the name of your sprite font file here instead of 'Score'.
            DatabaseSetup.setupDatabase();
        }

        //                  Nikolaj stuff
        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            DeltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds; // Runs deltatime.

            inputHandler.Execute(player); // makes the player do their actions


            //                  Nikolaj Stuff
            if (_nextState != null)
            {
                _currentState = _nextState;

                _nextState = null;
            }

            _currentState.Update(gameTime);

            _currentState.PostUpdate(gameTime);



            base.Update(gameTime);

            // Runs the update function for all GameObjects. 
            foreach (GameObject O in ObjectList)
            {
                O.Update(gameTime);
            }

            // Checks objects that are to be deleted, removes them from the list of objects and then clears the list.
            // Needs to be among the last things done.
            foreach (GameObject gameObject in deletionList)
            {
                ObjectList.Remove(gameObject);
            }
            deletionList.Clear();

            // Checks list of items to be added, adds them & clears its own list.
            // Needs to be among the last things done.
            foreach (GameObject gameObject in newObjectList)
            {
                ObjectList.Add(gameObject);
            }
            newObjectList.Clear();
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin();
            //background.Draw(gameTime, _spriteBatch);
            foreach (GameObject O in ObjectList)
            {
                O.Draw(gameTime, _spriteBatch);
            }
            //_spriteBatch.DrawString(font, displayText, new Vector2(100, 100), Color.Black);
            _spriteBatch.End();

            //                  Nikolaj stuff
            _currentState.Draw(gameTime, _spriteBatch);

            base.Draw(gameTime);
        }
    }
}
