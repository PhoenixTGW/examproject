﻿using Examproject.PlayerThings;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Examproject.Commands
{
    class InputHandler
    {
        private Dictionary<Keys, ICommand> keybinds = new Dictionary<Keys, ICommand>();

        /// <summary>
        /// The handler to add command keybinds.
        /// </summary>
        public InputHandler()
        {
            //keybinds.Add(Keys.D, new MoveCommand(new Vector2(1, 0)));
            //keybinds.Add(Keys.A, new MoveCommand(new Vector2(-1, 0)));
            //keybinds.Add(Keys.E, new ShieldCommand());
            keybinds.Add(Keys.F, new FightCommand());
        }

        /// <summary>
        /// Executes the commands in the player
        /// </summary>
        /// <param name="player">The player unit for the commands to be used on.</param>
        public void Execute(Player player)
        {
            KeyboardState keyState = Keyboard.GetState();
            foreach (Keys key in keybinds.Keys)
            {
                if (keyState.IsKeyDown(key))
                {
                    keybinds[key].Execute(player);
                }

            }
        }
    }
}
