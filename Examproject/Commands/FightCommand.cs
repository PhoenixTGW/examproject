﻿using Examproject.Combat;
using Examproject.PlayerThings;
using System.Threading;

namespace Examproject.Commands
{
    public class FightCommand : ICommand
    {
        Enemy foe = CombatStates.foe;
        int damage = CombatStates.weaponDamage;

        public FightCommand()
        {
        }

        public void Execute(Player player)
        {
            if (CombatStates.state == CombatStates.BattleState.PLAYERTURN)
            {
                foe.HitPoints -= damage;
                CombatStates.displayText = "You hit the  " + foe.Name + " for " + damage + " damage!";
                Thread.Sleep(2000);
                TurnOver.PlayerTurnOver();
            }
        }
    }
}
