﻿using Examproject.PlayerThings;

namespace Examproject.Commands
{
    public interface ICommand
    {
        void Execute(Player player);
    }
}
