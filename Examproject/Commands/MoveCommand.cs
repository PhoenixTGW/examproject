﻿using Examproject.PlayerThings;
using Microsoft.Xna.Framework;

namespace Examproject.Commands
{
    public class MoveCommand : ICommand
    {
        private Vector2 velocity;

        public MoveCommand(Vector2 velocity)
        {
            this.velocity = velocity;
        }

        public void Execute(Player player)
        {
            player.Move(velocity);
            if (velocity.X < 0f)
                player.FacingLeft = true;
            else if (velocity.X > 0f)
                player.FacingLeft = false;
        }
    }
}
