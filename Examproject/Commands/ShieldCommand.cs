﻿using Examproject.PlayerThings;

namespace Examproject.Commands
{
    class ShieldCommand : ICommand
    {
        public ShieldCommand()
        {

        }

        public void Execute(Player player)
        {
            if (!player.ShieldActive)
            {
                GameWorld.newObjectList.Add(new Shield(player.Position));
                player.ShieldActive = true;
                player.MovementDisabled = true;
            }
        }
    }
}