﻿using System;
using System.Data.SQLite;

namespace Examproject.Database
{
    internal class ViewAll
    {
        /// <summary>
        /// Sees all characters in the main database
        /// FIX: Rework this to work with an input of sorts so it can be universally referered to
        /// small todo: Update to work with 3 databases
        /// </summary>
        public static void View(string databaseName)
        {
            Console.WriteLine("");
            SQLiteConnection connection = new SQLiteConnection("Data Source=Adventure.db;Version=3;New=true");
            connection.Open();
            // command = new SQLiteCommand("CREATE TABLE IF NOT EXISTS units (Id INTEGER PRIMARY KEY, Name string, power int, endurance int);", connection);
            //command.ExecuteNonQuery();

            SQLiteCommand command = new SQLiteCommand($"SELECT * FROM {databaseName}", connection);
            SQLiteDataReader result = command.ExecuteReader();

            while (result.Read())
            {
                var id = result.GetInt32(0);
                var name = result.GetString(1);
                var power = result.GetInt32(2);
                var endurance = result.GetInt32(3);

                Console.WriteLine($"Id: {id} Name: {name} Power: {power} Endurance: {endurance}");
            }
            connection.Close();
            DatabaseMaintenance.MaintMenu();
        }
    }
}
