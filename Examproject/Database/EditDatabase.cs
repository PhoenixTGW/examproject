﻿using System;
using System.Data.SQLite;

namespace Examproject.Database
{
    internal class EditDatabase
    {
        /// <summary>
        /// Edit an existing entry in the main database
        /// FIX: Allows typing Null to not edit an entry
        /// </summary>
        public static void Edit(string databaseName)
        {
            string id;
            string string1;
            string string2;
            int int1;
            int int2;
            int int3;
            int int4;
            Console.WriteLine("");
            SQLiteConnection connection = new SQLiteConnection("Data Source=Adventure.db;Version=3;New=true");
            connection.Open();
            //var command = new SQLiteCommand("CREATE TABLE IF NOT EXISTS units (Id INTEGER PRIMARY KEY, Name string, power int, endurance int);", connection);
            //command.ExecuteNonQuery();
            if (databaseName == "weapon")
            {
                Console.Write("ID: ");
                id = Console.ReadLine();
                Console.Write("Damage: ");
                int1 = Convert.ToInt32(Console.ReadLine());
                Console.Write("special 1: ");
                string1 = Console.ReadLine();
                Console.Write("special 2: ");
                string2 = Console.ReadLine();
                SQLiteCommand command = new SQLiteCommand($"UPDATE units SET damage = {int1}, special1 = '{string1}', special2 = '{string2}' WHERE id = {id};", connection);
                command.ExecuteNonQuery();
            }
            else if (databaseName == "inventory")
            {
                Console.Write("ID: ");
                id = Console.ReadLine();
                Console.Write("Name: ");
                string1 = Console.ReadLine();
                Console.Write("Desc: ");
                string2 = Console.ReadLine();
                Console.Write("Value: ");
                int1 = Convert.ToInt32(Console.ReadLine());
                Console.Write("WeaopnID: ");
                int2 = Convert.ToInt32(Console.ReadLine());
                SQLiteCommand command = new SQLiteCommand($"UPDATE units SET name = '{string1}', desc = '{string2}', value = {int1}, WeaponID = {int2} WHERE id = {id};", connection);
                command.ExecuteNonQuery();
            }
            else if (databaseName == "stats")
            {
                Console.Write("ID: ");
                id = Console.ReadLine();
                Console.Write("Currency: ");
                int1 = Convert.ToInt32(Console.ReadLine());
                Console.Write("HP: ");
                int2 = Convert.ToInt32(Console.ReadLine());
                Console.Write("currentArea: ");
                int3 = Convert.ToInt32(Console.ReadLine());
                Console.Write("Selected weapon: ");
                int4 = Convert.ToInt32(Console.ReadLine());
                SQLiteCommand command = new SQLiteCommand($"UPDATE units SET WeaponSelected = '{int4}', currentArea = '{int3}', currency = {int1}, hp = {int2} WHERE id = {id};", connection);
                command.ExecuteNonQuery();
            }

            connection.Close();
            DatabaseMaintenance.MaintenanceMenu();
        }
    }
}
