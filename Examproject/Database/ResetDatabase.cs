﻿using System;
using System.Data.SQLite;

namespace Examproject.Database
{
    internal class ResetDatabase
    {
        /// NOTICE: OUTDATED CODE! OVERRIDDEN BY 'DatabaseSetup.cs'!


        /// <summary>
        /// Resets the entire database.
        /// small todo: Make it confirm before doing it
        /// </summary>
        public static void Reset()
        {
            Console.WriteLine("");
            SQLiteConnection connection = new SQLiteConnection("Data Source=Adventure.db;Version=3;New=true");
            connection.Open();
            SQLiteCommand command = new SQLiteCommand("DROP TABLE IF EXISTS units", connection);
            command.ExecuteNonQuery();
            command = new SQLiteCommand("CREATE TABLE IF NOT EXISTS units (Id INTEGER PRIMARY KEY, Name string, power int, endurance int);", connection);
            command.ExecuteNonQuery();

            command = new SQLiteCommand("INSERT INTO units VALUES (null, 'Daenerys Targaryen', '35', '74');", connection);
            command.ExecuteNonQuery();

            command = new SQLiteCommand("INSERT INTO units VALUES (null, 'Tyrion Lannister', '74', '44');", connection);
            command.ExecuteNonQuery();

            command = new SQLiteCommand("INSERT INTO units VALUES (null, 'Jon Snow', '11', '99');", connection);
            command.ExecuteNonQuery();

            command = new SQLiteCommand("SELECT * FROM units", connection);
            SQLiteDataReader result = command.ExecuteReader();

            while (result.Read())
            {
                var id = result.GetInt32(0);
                var name = result.GetString(1);
                var power = result.GetInt32(2);
                var endurance = result.GetInt32(3);

                Console.WriteLine($"Id: {id} Name: {name} Power: {power} Endurance: {endurance}");
            }
            connection.Close();
            DatabaseMaintenance.MaintMenu();
        }
    }
}
