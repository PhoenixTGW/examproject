﻿using System;
using System.Data.SQLite;

namespace Examproject.Database
{
    internal class AddDatabase
    {

        /// <summary>
        /// Opens a new set of commands to add entries to the main karakter database.
        /// small todo: Update to work with 3 databases
        /// </summary>
        public static void Add(string databaseName)
        {
            string string1;
            string string2;
            int int1;
            int int2;
            int int3;
            int int4;
            Console.WriteLine("");
            SQLiteConnection connection = new SQLiteConnection("Data Source=Adventure.db;Version=3;New=true");
            connection.Open();
            SQLiteCommand command = new SQLiteCommand($"CREATE TABLE IF NOT EXISTS {databaseName} (Id INTEGER PRIMARY KEY, Name string, power int, endurance int);", connection);
            command.ExecuteNonQuery();

            if (databaseName == "weapon")
            {
                Console.Write("Damage: ");
                int1 = Convert.ToInt32(Console.ReadLine());
                Console.Write("special 1: ");
                string1 = Console.ReadLine();
                Console.Write("special 2: ");
                string2 = Console.ReadLine();
                command = new SQLiteCommand($"INSERT INTO weapon VALUES (null, '{int1}', {string1}, {string2});", connection);
                command.ExecuteNonQuery();
            }
            else if (databaseName == "inventory")
            {
                Console.Write("Name: ");
                string1 = Console.ReadLine();
                Console.Write("Desc: ");
                string2 = Console.ReadLine();
                Console.Write("Value: ");
                int1 = Convert.ToInt32(Console.ReadLine());
                Console.Write("WeaopnID: ");
                int2 = Convert.ToInt32(Console.ReadLine());
                command = new SQLiteCommand($"INSERT INTO inventory VALUES (null, '{string1}', {string2}, {int1}, {int2});", connection);
                command.ExecuteNonQuery();
            }
            else if (databaseName == "stats")
            {
                Console.Write("Currency: ");
                int1 = Convert.ToInt32(Console.ReadLine());
                Console.Write("HP: ");
                int2 = Convert.ToInt32(Console.ReadLine());
                Console.Write("currentArea: ");
                int3 = Convert.ToInt32(Console.ReadLine());
                Console.Write("Selected weapon: ");
                int4 = Convert.ToInt32(Console.ReadLine());
                command = new SQLiteCommand($"INSERT INTO stats VALUES (null, '{int1}', {int2}, {int3}, {int4});", connection);
                command.ExecuteNonQuery();
            }

            Console.Write("Name: ");
            string name = Console.ReadLine();
            Console.Write("Power: ");
            int power = Convert.ToInt32(Console.ReadLine());
            Console.Write("Endurance: ");
            int endurance = Convert.ToInt32(Console.ReadLine());
            command = new SQLiteCommand($"INSERT INTO units VALUES (null, '{name}', {power}, {endurance});", connection);
            command.ExecuteNonQuery();
            connection.Close();
            DatabaseMaintenance.MaintenanceMenu();
        }
    }
}
