﻿using System;
using System.Data.SQLite;

namespace Examproject.Database
{
    internal class DeleteDatabaseEntry
    {
        /// <summary>
        /// Use to delete an entry from the main database
        /// </summary>
        public static void Delete(string databaseName)
        {
            Console.WriteLine("");
            SQLiteConnection connection = new SQLiteConnection("Data Source=Adventure.db;Version=3;New=true");
            connection.Open();
            SQLiteCommand command = new SQLiteCommand($"CREATE TABLE IF NOT EXISTS {databaseName} (Id INTEGER PRIMARY KEY, Name string, power int, endurance int);", connection);
            command.ExecuteNonQuery();
            Console.Write("Entry to delete: ");
            string deletionEntry = Console.ReadLine();
            command = new SQLiteCommand($"DELETE FROM units WHERE Id={deletionEntry};", connection);
            command.ExecuteNonQuery();
            connection.Close();
            DatabaseMaintenance.MaintenanceMenu();
        }
    }
}
