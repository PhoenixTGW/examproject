﻿using System;

namespace Examproject.Database
{
    internal class DatabaseMaintenance
    {
        /// <summary>
        /// The maintenance menu. Use to begin database maintenance
        /// </summary>
        public static void MaintenanceMenu()
        {
            Console.Clear();
            Console.WriteLine("MAINTENANCE MENU");
            Console.WriteLine("----------------");
            Console.WriteLine("1: View all database entries");
            Console.WriteLine("2: Edit an entry");
            Console.WriteLine("3: Add an entry");
            Console.WriteLine("4: Delete an entry");
            Console.WriteLine("5: Reset database");
            Console.WriteLine("0: Exit");
            MaintMenu();
        }

        public static void MaintMenu()
        {
            string fileSelect;
            string key = Console.ReadKey().KeyChar.ToString();
            switch (key)
            {
                case "1":
                    Console.WriteLine("----------------");
                    Console.WriteLine("Select file to edit\n1: weapon\n2: inventory\n3: stats");
                    fileSelect = Console.ReadKey().KeyChar.ToString();
                    switch (fileSelect)
                    {
                        case "1":
                            ViewAll.View("weapon");
                            break;
                        case "2":
                            ViewAll.View("inventory");
                            break;
                        case "3":
                            ViewAll.View("stats");
                            break;
                    }
                    break;
                case "2":
                    Console.WriteLine("----------------");
                    Console.WriteLine("Select file to edit\n1: weapon\n2: inventory\n3: stats");
                    fileSelect = Console.ReadKey().KeyChar.ToString();
                    switch (fileSelect)
                    {
                        case "1":
                            EditDatabase.Edit("weapon");
                            break;
                        case "2":
                            EditDatabase.Edit("inventory");
                            break;
                        case "3":
                            EditDatabase.Edit("stats");
                            break;
                    }
                    break;
                case "3":
                    Console.WriteLine("----------------");
                    Console.WriteLine("Select file to edit\n1: weapon\n2: inventory\n3: stats");
                    fileSelect = Console.ReadKey().KeyChar.ToString();
                    switch (fileSelect)
                    {
                        case "1":
                            AddDatabase.Add("weapon");
                            break;
                        case "2":
                            AddDatabase.Add("inventory");
                            break;
                        case "3":
                            AddDatabase.Add("stats");
                            break;
                    }
                    break;
                case "4":
                    Console.WriteLine("----------------");
                    Console.WriteLine("Select file to edit\n1: weapon\n2: inventory\n3: stats");
                    fileSelect = Console.ReadKey().KeyChar.ToString();
                    switch (fileSelect)
                    {
                        case "1":
                            DeleteDatabaseEntry.Delete("weapon");
                            break;
                        case "2":
                            DeleteDatabaseEntry.Delete("inventory");
                            break;
                        case "3":
                            DeleteDatabaseEntry.Delete("stats");
                            break;
                    }
                    break;
                case "5":
                    DatabaseSetup.setupDatabase();
                    break;
                case "0":
                    break;
            }
        }
    }
}
