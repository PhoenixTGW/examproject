﻿using System;
using System.Data.SQLite;

namespace Examproject.Database
{
    class DatabaseSetup
    {
        public static void setupDatabase()
        {
            Console.WriteLine("");
            SQLiteConnection connection = new SQLiteConnection("Data Source=Adventure.db;Version=3;New=true");
            connection.Open();

            SQLiteCommand weapon = new SQLiteCommand("DROP TABLE IF EXISTS weapon", connection);
            weapon.ExecuteNonQuery();
            weapon = new SQLiteCommand("CREATE TABLE IF NOT EXISTS weapon (Id INTEGER PRIMARY KEY, damage int, special1 string, special2 int);", connection);
            weapon.ExecuteNonQuery();
            weapon = new SQLiteCommand("INSERT INTO weapon VALUES (null, 7, null, null);", connection);
            weapon.ExecuteNonQuery();

            SQLiteCommand inventory = new SQLiteCommand("DROP TABLE IF EXISTS inventory", connection);
            inventory.ExecuteNonQuery();
            inventory = new SQLiteCommand("CREATE TABLE IF NOT EXISTS inventory (Id INTEGER PRIMARY KEY, name string, desc string, value int, WeaponID int, FOREIGN KEY (WeaponID) REFERENCES weapon(Id));", connection);
            inventory.ExecuteNonQuery();

            inventory = new SQLiteCommand("INSERT INTO inventory VALUES (null, 'rusty old sword', 'a rusty sword', 5, 1);", connection);
            inventory.ExecuteNonQuery();


            SQLiteCommand stats = new SQLiteCommand("DROP TABLE IF EXISTS stats", connection);
            stats.ExecuteNonQuery();
            stats = new SQLiteCommand("CREATE TABLE IF NOT EXISTS stats (Id INTEGER PRIMARY KEY, currency int, hp int, currentArea int, WeaponSelected int, FOREIGN KEY (WeaponSelected) REFERENCES weapon(Id));", connection);
            stats.ExecuteNonQuery();
            stats = new SQLiteCommand("INSERT INTO stats VALUES (null, 0, 20, 1, 1);", connection);
            stats.ExecuteNonQuery();
            inventory = new SQLiteCommand("SELECT * FROM inventory", connection);
            SQLiteDataReader result = inventory.ExecuteReader();

            while (result.Read())
            {
                var id = result.GetInt32(0);
                var name = result.GetString(1);
                var desc = result.GetString(2);
                var value = result.GetInt32(3);

                Console.WriteLine($"Id: {id} Name: {name} description: {desc} value: {value}");
            }
            connection.Close();
        }
    }
}
