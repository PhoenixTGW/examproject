﻿namespace Examproject.Combat
{
    /// <summary>
    /// Controls what happens when the various type of turns are over.
    /// If conditions exist for what turn it changes to, apply it here.
    /// </summary>
    static class TurnOver
    {
        public static void PlayerTurnOver()
        {
            if (CombatStates.foe.HitPoints >= 1)
            {
                CombatStates.EnemyTurn();
            }
            else if (CombatStates.foe.HitPoints <= 0)
            {
                CombatStates.Won();
            }
        }

        public static void EnemyTurnOver()
        {
            if (CombatStates.PlayerHP <= 0)
            {
                CombatStates.Lost();
            }
            if (CombatStates.foe.HitPoints >= 1 && CombatStates.PlayerHP >= 1)
            {
                CombatStates.PlayerTurn();
            }
            else if (CombatStates.foe.HitPoints <= 0)
            {
                CombatStates.Won();
            }
        }

        public static void WonOver()
        {
            CombatStates.state = CombatStates.BattleState.STORY;
        }

        public static void LostOver()
        {

        }
    }
}
