﻿namespace Examproject.Combat
{
    public class Enemy
    {
        public string Name = "Enemy";
        public int HitPoints = 10;
        public int Damage = 2;

        public Enemy(string name, int hitPoints, int damage)
        {
            Name = name;
            HitPoints = hitPoints;
            Damage = damage;
        }
    }
}
