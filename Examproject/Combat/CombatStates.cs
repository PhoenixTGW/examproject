﻿using System;
using System.Data.SQLite;
using System.Threading;

namespace Examproject.Combat
{
    /// <summary>
    /// The primary combat system.
    /// </summary>
    static class CombatStates
    {
        #region setup
        public enum BattleState { START, PLAYERTURN, ENEMYTURN, WON, LOST, STORY }
        public static BattleState state = BattleState.STORY; // Combat states. Change using TurnOver.cs

        public static string displayText;

        public static int PlayerHP;
        private static int currentWeapon;
        public static int weaponDamage = 1;

        public static Enemy foe;

        /// <summary>
        /// Sets up the battle system. Call this to initiate combat.
        /// </summary>
        public static void SetupBattle()
        {
            foe = new Enemy("Foe", 10, 2);
            state = BattleState.START;
            #region database
            SQLiteConnection connection = new SQLiteConnection("Data Source=Adventure.db;Version=3;New=true");
            connection.Open();
            SQLiteCommand stats = new SQLiteCommand("CREATE TABLE IF NOT EXISTS stats (Id INTEGER PRIMARY KEY, currency int, hp int,currentArea int, WeaponSelected int, FOREIGN KEY (WeaponSelected) REFERENCES weapon(Id));", connection);
            stats.ExecuteNonQuery();
            stats = new SQLiteCommand("SELECT WeaponSelected FROM inventory", connection);
            SQLiteDataReader result = stats.ExecuteReader();

            while (result.Read())
            {
                currentWeapon = result.GetInt32(0);

                Console.WriteLine($"CurrentWeapon: {currentWeapon}");
            }
            connection.Close();
            #endregion
            displayText = "Combat Initiating.";
            Thread.Sleep(2000);
            PlayerTurn();
            // CRIT: make it display the text
        }
        #endregion
        #region turns
        public static void PlayerTurn()
        {
            displayText = "Pick your move.\n";
            state = BattleState.PLAYERTURN;
            // We wait for the command now.
        }

        public static void EnemyTurn()
        {
            state = BattleState.ENEMYTURN;
            Random rnd = new Random();
            int damage = rnd.Next(1, 3);
            PlayerHP -= damage;
            displayText = "The " + foe.Name + " attacks for " + damage + " damage!";
            Thread.Sleep(2000);
            TurnOver.EnemyTurnOver();
        }

        public static void Won()
        {
            state = BattleState.WON;
            displayText = "The " + foe.Name + " is dead!";
            Thread.Sleep(10000);
            TurnOver.WonOver();
        }

        public static void Lost()
        {
            state = BattleState.LOST;
            displayText = "YOU DIED!";
            Thread.Sleep(10000);
            TurnOver.LostOver();
        }
        #endregion
    }
}
