﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Examproject.PlayerThings
{
    public class Shield : GameObject
    {
        private int lifespan;
        private readonly int halfseconds = 1;
        public Shield(Vector2 newPos)
        {
            Sprite = GameWorld.contentFix.Load<Texture2D>("Still");
            LoadContent(GameWorld.contentFix);
            if (GameWorld.player.FacingLeft)
                newPos.X -= 30;
            else
                newPos.X += 30;
            Position = newPos;
            lifespan = (30 * halfseconds);
        }
        public override void LoadContent(ContentManager content)
        {
            Sprite = content.Load<Texture2D>("Still");
            base.LoadContent(content);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            // spriteBatch.Begin();
            base.Draw(gameTime, spriteBatch);
            // spriteBatch.End();
        }
        public override void Update(GameTime gameTime)
        {

            //if (GameWorld.player.ShieldActive == false)
            //{
            //    GameWorld.deletionList.Add(this);
            //}
            lifespan -= 1;
            if (lifespan <= 0)
            {
                GameWorld.deletionList.Add(this);
                GameWorld.player.ShieldActive = false;
                GameWorld.player.MovementDisabled = false;
            }

        }
    }
}
