﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Examproject.PlayerThings
{
    public class Player : Movable
    {
        public bool ShieldActive = false;
        public bool FacingLeft = false;
        public Player(Vector2 startPos)
        {
            speed = 100;
            // canShoot = true;
            Position = startPos;
        }

        public override void LoadContent(ContentManager content)
        {
            Sprite = content.Load<Texture2D>("ScreenBorder");
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
        }
    }
}
