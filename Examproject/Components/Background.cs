﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Examproject.Components
{
    /// <summary>
    /// The background component for the game, to display a background.
    /// </summary>
    public class Background
    {
        protected Texture2D Sprite;
        public Vector2 Position { get; set; } = new Vector2(-200f, -50f);
        public void LoadContent(ContentManager content)
        {
            Sprite = content.Load<Texture2D>("background/MainMenuArt");
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Sprite, Position, Color.White);
        }
    }
}
