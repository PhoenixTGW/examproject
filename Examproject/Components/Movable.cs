﻿using Microsoft.Xna.Framework;

namespace Examproject.PlayerThings
{
    public abstract class Movable : GameObject
    {
        protected float speed;
        protected bool IsTouchingGround = false;
        public bool MovementDisabled = false;
        public void Move(Vector2 velocity)
        {
            if (!MovementDisabled)
            {
                if (velocity != Vector2.Zero)
                {
                    velocity.Normalize();
                }

                velocity *= speed;

                Position += (velocity * GameWorld.DeltaTime);
            }
            // GameObject.Transform.Translate(velocity * GameWorld.Instance.DeltaTime);

        }
    }
}
