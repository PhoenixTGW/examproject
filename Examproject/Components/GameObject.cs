﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Examproject.PlayerThings
{
    public abstract class GameObject
    {
        public Vector2 Position { get; set; } = new Vector2(0f, 0f);
        protected Texture2D Sprite;
        public virtual Rectangle collisionBox { get; }
        public bool IsQueuedForDeletion { get; set; } = false;
        public bool IsGround { get; set; } = false;

        public virtual void Update(GameTime gameTime)
        {

        }
        public virtual void LoadContent(ContentManager content)
        {

        }
        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Sprite, Position, Color.White);
        }
        public virtual void OnCollision(GameObject other)
        {

        }

        public Rectangle CollisionBox
        {
            get
            {
                return new Rectangle(
                    (int)(Position.X),
                    (int)(Position.Y),
                    Sprite.Width,
                    Sprite.Height
                    );
            }
        }

        public virtual bool CheckCollision(GameObject other)
        {
            if (IsColliding(other))
            {
                OnCollision(other);
                return true;
            }
            return false;
        }
        public virtual bool IsColliding(GameObject other)
        {
            return IsQueuedForDeletion || other.IsQueuedForDeletion ? false : collisionBox.Intersects(other.collisionBox);
        }
    }
}
