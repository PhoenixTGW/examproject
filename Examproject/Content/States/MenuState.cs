﻿using Examproject.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Examproject.States
{
    public class MenuState : State
    {
        private List<Component> _components;

        public MenuState(GameWorld game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            var backgroundTexture = _content.Load<Texture2D>("background/MainMenuArt");
            var textBoxTexture = _content.Load<Texture2D>("Controls/DescribtionBox");
            var buttonTexture = _content.Load<Texture2D>("Controls/Button_150_50");
            var buttonFont = _content.Load<SpriteFont>("Fonts/Font");


            var Menu_BackgroundPic = new BackgroundPic(backgroundTexture)
            {
                Position = new Vector2(0, 0),
            };


            var Menu_Describtion = new TextBox(textBoxTexture, buttonFont)
            {
                Position = new Vector2(250, 50),
                Text = "The Plaza, being the heart of the city means it is likely to be very busy," + " and has therefore been constructed as a large open place with thick roads",
            };


            var newGameButton = new Button(buttonTexture, buttonFont)
            {
                Position = new Vector2(100, 300),
                Text = "New Game",
            };
            newGameButton.Click += NewGameButton_Click;

            var loadGameButton = new Button(buttonTexture, buttonFont)
            {
                Position = new Vector2(100, 375),
                Text = "Load Game",
            };
            loadGameButton.Click += LoadGameButton_Click;

            var quitGameButton = new Button(buttonTexture, buttonFont)
            {
                Position = new Vector2(100, 650),
                Text = "Quit Game",
            };
            quitGameButton.Click += QuitGameButton_Click;


            _components = new List<Component>()
            {
                Menu_BackgroundPic,
                Menu_Describtion,
                newGameButton,
                loadGameButton,
                quitGameButton,
            };
        }


        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            foreach (var component in _components)
                component.Draw(gameTime, spriteBatch);

            spriteBatch.End();
        }

        private void LoadGameButton_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Load Game");
        }

        private void NewGameButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new GameState_Plaza(_game, _graphicsDevice, _content));
        }
        public override void PostUpdate(GameTime gameTime)
        {
            // Remove sprites if they're not needed
        }

        public override void Update(GameTime gameTime)
        {
            foreach (var component in _components)
                component.Update(gameTime);
        }

        private void QuitGameButton_Click(object sender, EventArgs e)
        {
            _game.Exit();
        }
    }
}
