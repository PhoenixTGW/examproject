﻿using Examproject.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Examproject.States
{
    public class GameState_NoticeBoard : State
    {
        private List<Component> _components;
            //"This is the Notice Board." +
            //"Placed right by the fountain in the Plaza, it functions as a convenient place to share news" +
            //"and is also commonly used as a way for lesser quests to be seen by any bypassing";

        public GameState_NoticeBoard(GameWorld game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            var buttonTexture = _content.Load<Texture2D>("Controls/Button_150_50");
            var buttonFont = _content.Load<SpriteFont>("Fonts/Font");


            //if DoingQuest1 = false
            //{
                var noticeBoard_AcceptQuest1 = new Button(buttonTexture, buttonFont)
                {
                    Position = new Vector2(650, 300),
                    Text = "Take Quest: chase monsters",
                };
                noticeBoard_AcceptQuest1.Click += NoticeBoard_AcceptQuest1_Click;
            //}

            //if DoingQuest2 = false
            //{
                var noticeBoard_AcceptQuest2 = new Button(buttonTexture, buttonFont)
                {
                    Position = new Vector2(650, 425),
                    Text = "Take Quest: gather herbs",
                };
                noticeBoard_AcceptQuest2.Click += NoticeBoard_AcceptQuest2_Click;
            //}

            var noticeBoard_LeaveButton = new Button(buttonTexture, buttonFont)
            {
                Position = new Vector2(650, 550),
                Text = "Leave the Notice Board",
            };
            noticeBoard_LeaveButton.Click += NoticeBoard_LeaveButton_Click;

            var toMenuButton = new Button(buttonTexture, buttonFont)
            {
                Position = new Vector2(650, 675),
                Text = "Quit to Menu",
            };
            toMenuButton.Click += ToMenuButton_Click;


            _components = new List<Component>()
            {
                noticeBoard_AcceptQuest1,
                noticeBoard_AcceptQuest2,
                noticeBoard_LeaveButton,
                toMenuButton,
            };
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            //background.Draw(gameTime, _spriteBatch);
            //foreach (GameObject O in ObjectList)
            //{
            //    O.Draw(gameTime, _spriteBatch);
            //}

            foreach (var component in _components)
                component.Draw(gameTime, spriteBatch);

            spriteBatch.End();
        }

        private void NoticeBoard_AcceptQuest1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Accepted Quest : chase monsters");
        }

        private void NoticeBoard_AcceptQuest2_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Accepted Quest : gather herbs");
        }
        private void NoticeBoard_LeaveButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new GameState_Plaza(_game, _graphicsDevice, _content));
        }

        private void ToMenuButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new MenuState(_game, _graphicsDevice, _content));
        }

        public override void PostUpdate(GameTime gameTime)
        {
            // Remove sprites if they're not needed
        }

        public override void Update(GameTime gameTime)
        {
            foreach (var component in _components)
                component.Update(gameTime);
        }
    }
}
