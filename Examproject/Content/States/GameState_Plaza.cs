﻿using Examproject.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Examproject.States
{
    public class GameState_Plaza : State
    {
        private List<Component> _components;

        public GameState_Plaza(GameWorld game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            var backgroundTexture = _content.Load<Texture2D>("background/PlazaBG");
            var textBoxTexture = _content.Load<Texture2D>("Controls/DescribtionBox");
            var buttonTexture = _content.Load<Texture2D>("Controls/Button_150_50");
            var buttonFont = _content.Load<SpriteFont>("Fonts/Font");


            var Plaza_BackgroundPic = new BackgroundPic(backgroundTexture)
            {
                Position = new Vector2(0, 0),
            };


            var Plaza_Describtion = new TextBox(textBoxTexture, buttonFont)
            {
                Position = new Vector2(250, 50),
                Text = "The Plaza, being the heart of the city means it is likely to be very busy," + " and has therefore been constructed as a large open place with thick roads",
            };


            var plaza_GoToForrestButton = new Button(buttonTexture, buttonFont)
            {
                Position = new Vector2(650, 300),
                Text = "Go to the Forrest",
            };
            plaza_GoToForrestButton.Click += Plaza_GoToForrestButton_Click;

            var plaza_NoticeBoardButton = new Button(buttonTexture, buttonFont)
            {
                Position = new Vector2(650, 425),
                Text = "Check Notice Board",
            };
            plaza_NoticeBoardButton.Click += Plaza_NoticeBoardButton_Click;

            var toMenuButton = new Button(buttonTexture, buttonFont)
            {
                Position = new Vector2(650, 675),
                Text = "Quit to Menu",
            };
            toMenuButton.Click += ToMenuButton_Click;


            _components = new List<Component>()
            {
                Plaza_BackgroundPic,
                Plaza_Describtion,
                plaza_GoToForrestButton,
                plaza_NoticeBoardButton,
                toMenuButton,
            };
        }


        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            //background.Draw(gameTime, _spriteBatch);
            foreach (var component in _components)
                component.Draw(gameTime, spriteBatch);

            spriteBatch.End();
        }

        private void Plaza_NoticeBoardButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new GameState_NoticeBoard(_game, _graphicsDevice, _content));
        }

        private void Plaza_GoToForrestButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new GameState_Forrest(_game, _graphicsDevice, _content));
        }
        public override void PostUpdate(GameTime gameTime)
        {
            // Remove sprites if they're not needed
        }

        public override void Update(GameTime gameTime)
        {
            foreach (var component in _components)
                component.Update(gameTime);
        }

        private void ToMenuButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new MenuState(_game, _graphicsDevice, _content));
        }
    }
}
