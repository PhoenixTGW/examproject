﻿using Examproject.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Examproject.States
{
    public class GameState_Forrest : State
    {
        private List<Component> _components;

        public GameState_Forrest(GameWorld game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            var buttonTexture = _content.Load<Texture2D>("Controls/Button_150_50");
            var buttonFont = _content.Load<SpriteFont>("Fonts/Font");

            var plaza_GoToForrestButton = new Button(buttonTexture, buttonFont)
            {
                Position = new Vector2(650, 300),
                Text = "Search area",
            };
            plaza_GoToForrestButton.Click += Plaza_GoToForrestButton_Click;

            var plaza_NoticeBoardButton = new Button(buttonTexture, buttonFont)
            {
                Position = new Vector2(650, 425),
                Text = "Go deeper into the Forrest",
            };
            plaza_NoticeBoardButton.Click += Plaza_NoticeBoardButton_Click;

            var toMenuButton = new Button(buttonTexture, buttonFont)
            {
                Position = new Vector2(650, 675),
                Text = "Quit to Menu",
            };
            toMenuButton.Click += ToMenuButton_Click;


            _components = new List<Component>()
            {
                plaza_GoToForrestButton,
                plaza_NoticeBoardButton,
                toMenuButton,
            };
        }


        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            foreach (var component in _components)
                component.Draw(gameTime, spriteBatch);

            spriteBatch.End();
        }

        private void Plaza_NoticeBoardButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new GameState_NoticeBoard(_game, _graphicsDevice, _content));
        }

        private void Plaza_GoToForrestButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new GameState_Forrest(_game, _graphicsDevice, _content));
        }
        public override void PostUpdate(GameTime gameTime)
        {
            // Remove sprites if they're not needed
        }

        public override void Update(GameTime gameTime)
        {
            foreach (var component in _components)
                component.Update(gameTime);
        }

        private void ToMenuButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new MenuState(_game, _graphicsDevice, _content));
        }
    }
}